require_relative '../app'
require 'rack/test'

# setup test environment
set :environment, :test
set :run, false
set :raise_errors, true
set :logging, false

def app
  App # our modular sinatra app
end

RSpec.configure do |config|
  config.include Rack::Test::Methods
end