require 'sinatra'
require 'sinatra/base'
require "sinatra/reloader" if development?

require_relative './routes'


## Uncomment for mongo functionality
#require 'fileutils'
#require 'json'
#require 'mongoid'

#require_relative './models'
#Mongoid.load!(File.dirname(__FILE__) + '/config/mongoid.yml')



class App < Sinatra::Base
	configure :development do
		register Sinatra::Reloader
	end

	configure :test do
	end

	configure :production do
	end

	
	use Routes

end