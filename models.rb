## Uncomment for mongo functionality

# module ModelHelpers 
# 	def make_slug(title)
# 		title.downcase.gsub(/ /, '_').gsub(/[^a-z0-9_]/, '').squeeze('_')
# 	end
# end


# class Peep
# 	include Mongoid::Document
# 	include ModelHelpers

# 	validates_presence_of :username, :password
# 	#attr_accessible :username

# 	field :username
# 	field :default_pic, default: '/images/ddog.png'
	
# 	embeds_many :pics

# 	def to_s
# 		"#{username}, #{_id}"
# 	end

# end

# class Pic
# 	include Mongoid::Document
	
# 	validates_presence_of :filename

# 	field :filename
# 	field :caption

# 	embedded_in :peep

# 	def to_s
# 		"#{filename}"
# 	end
# end