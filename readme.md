# Sinatra Skeleton
Includes with Gemfile, rspec, mongoid, Twitter Bootstrap v2.0.3, jQuery, jQuery 1.7.2, fancybox 2.0.6, thin

## Usage
Run 'bundle install' 

To run specs, use 'rake spec'

To start the development web server, use 'thin start'
Any code changes will auto-reload the application

MongoDB functionality is via mongoid. Uncomment the fields where highlighted